class PresentationPublisher
  def self.publish_to presentation, body
    Net::HTTP.post_form(URI.parse(self.config[:faye_server]), message(presentation, body).to_json)
  end
  def self.config
    @config ||= {}
  end
  def self.channel presentation
    if presentation
      "/presentation_#{presentation.id}/current_page"
    else
      "/presentation/current_page"
    end
  end
  def self.message presentation, body
    { :channel => channel(presentation),
      :data => body }
  end
end

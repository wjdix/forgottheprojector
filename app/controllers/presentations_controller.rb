class PresentationsController < ApplicationController
  def index
  end

  def show
  end

  def update
    PresentationPublisher.publish({:current_page => params[:current_page]})
    render :nothing => true
  end

end

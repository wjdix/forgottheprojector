# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/
$( -> 
  faye = new Faye.Client("http://simple-moon-2356.herokuapp.com/faye")
  faye.subscribe '/presentation/current_page', (data) ->
    alert data.current_page
    $.deck 'go', data.current_page
)

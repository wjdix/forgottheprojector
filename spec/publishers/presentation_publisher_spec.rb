require 'spec_helper'

describe PresentationPublisher do
  let(:presentation){ stub(:id => 4) }
  describe "channel" do
    it "builds the channel name based on id of presentation" do
      PresentationPublisher.channel(presentation).should == '/presentation_4/current_page'
    end
    it "builds a channel name if presentation is nil" do
      PresentationPublisher.channel(nil).should == '/presentation/current_page'
    end
  end
  describe "message" do
    it 'builds message based on channel and data' do
      PresentationPublisher.should_receive(:channel).with(presentation).
        and_return '/presentation_4/current_page'
      PresentationPublisher.message(presentation, {:current_page => 4}).should eq(
        :channel => '/presentation_4/current_page',
        :data => {:current_page => 4 }
      )
    end
  end
  describe "publish_to" do
    let(:message){ stub }
    it "makes an http request post to the correct uri" do
      PresentationPublisher.config[:faye_server] = "http://localhost"
      Net::HTTP.should_receive(:post_form).with(URI.parse("http://localhost"), anything())
      PresentationPublisher.publish_to(presentation, {})
    end
    it "makes an http request with the message as json" do
      PresentationPublisher.stub(:message).and_return message
      message.stub(:to_json).and_return 'json_message'
      Net::HTTP.should_receive(:post_form).with(anything(), 'json_message')
      PresentationPublisher.publish_to(presentation, {:current_page => 3})
    end
  end
end

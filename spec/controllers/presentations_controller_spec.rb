require 'spec_helper'
class  PresentationPublisher  ; end
describe PresentationsController do
  describe "PUT update" do
    it "publishs on Presentation publisher" do
      PresentationPublisher.should_receive(:publish).with({:current_page => "1"})
      put :update, :current_page => 1
    end
  end
end
